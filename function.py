from docx import Document
from docx.shared import Inches
from docx.shared import Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import RGBColor
import datetime
from smtplib import SMTP_SSL
from email.mime.multipart import MIMEMultipart
from email.mime.multipart import MIMEBase
from email import encoders
import os
import ftplib


def make_mail(filename, address):
    basename = os.path.basename(filename)

    # Составляем вложение
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(filename, "rb").read())
    encoders.encode_base64(part)

    part.add_header('Content-Disposition', 'attachment; filename="%s"' % basename)

    # Формируем строку письма
    msg = MIMEMultipart()
    msg['From'] = address
    msg['To'] = address
    msg['Subject'] = 'Диплом за победу в игре "Что? Где? Когда?"'
    msg.attach(part)

    # Отправляем сообщение
    smtp = SMTP_SSL()
    smtp.connect('smtp.yandex.ru')
    #server = smtplib.SMTP('smtp.example.com', 25)
    #server.connect("smtp.example.com",465)
    smtp.login('AliceYx@yandex.ru', 'avonahok')
    smtp.sendmail('AliceYx@yandex.ru', address, msg.as_string())
    smtp.quit()


def make_diplom(team_name, player_list, session_id, players_):
    #здесь формируется диплом
    #Объявляются параграфы и в них пишется текст
    now = datetime.datetime.now()
    data = '.'.join(list(map(lambda x: str(x), [now.day, now.month, now.year])))# + ' в ' + ':'.join(
       # list(map(lambda x: str(x) if len(str(x)) == 2 else '0' + str(x), [now.hour, now.minute])))

    document = Document()

    paragraph1 = document.add_paragraph()
    paragraph1_format = paragraph1.paragraph_format
    paragraph1_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

    head = paragraph1.add_run('Диплом абсолютного победителя игры “Что? Где? Когда?”')
    head.name = "Microsoft JhengHei Light"
    head.bold = True
    head.font.color.rgb = RGBColor(99, 37, 37)
    head.font.size = Pt(25)

    paragraph2 = document.add_paragraph()
    paragraph2_format = paragraph2.paragraph_format
    paragraph2_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

    data_string = paragraph2.add_run('\nИгра состоялась ' + data)
    data_string.name = "Microsoft JhengHei Light"
    data_string.font.size = Pt(15)
    data_string.font.color.rgb = RGBColor(61, 24, 24)

    paragraph3 = document.add_paragraph()
    paragraph3_format = paragraph3.paragraph_format
    paragraph3_format.alignment = WD_ALIGN_PARAGRAPH.LEFT

    team_win_string = paragraph3.add_run(
        '\n\nДипломом абсолютного победителя игры \nнаграждается команда "' + team_name + '"')
    team_win_string.name = "Microsoft JhengHei Light"
    team_win_string.font.size = Pt(20)
    team_win_string.font.color.rgb = RGBColor(61, 24, 24)

    if player_list:
        paragraph4 = document.add_paragraph()
        paragraph4_format = paragraph4.paragraph_format
        paragraph4_format.alignment = WD_ALIGN_PARAGRAPH.LEFT

        players = paragraph4.add_run('\nСостав команды:')
        players.name = "Microsoft JhengHei Light"
        players.font.size = Pt(18)
        players.font.color.rgb = RGBColor(61, 24, 24)
        for name in player_list:
            player = paragraph4.add_run('\n   - ' + name)
            player.name = "Microsoft JhengHei Light"
            player.font.size = Pt(18)
            player.font.color.rgb = RGBColor(61, 24, 24)

    paragraph5 = document.add_paragraph()
    paragraph5_format = paragraph5.paragraph_format
    paragraph5_format.alignment = WD_ALIGN_PARAGRAPH.RIGHT
    inf = paragraph5.add_run('\n' * (5 - len(player_list)) + 'Уникальный идентификатов сессии:\n' + session_id)
    inf.name = "Microsoft JhengHei Light"
    inf.font.size = Pt(18)
    inf.font.color.rgb = RGBColor(61, 24, 24)

    document.add_picture("mysite/line_logotip_.jpg", width=Inches(6.3))

    document.save('users_diploms/' + players_ + '.docx')


def to_make_url(players):
    host = 'ftp.kohanova.nichost.ru'
    ftp_user = 'kohanova_order'
    ftp_password = 'xLBHz_8N'
    filename = 'users_diploms/' + players + '.docx'
    con = ftplib.FTP(host)
    con.login(ftp_user, ftp_password)
    con.cwd("/diploms")
    f = open(filename, "rb")
    con.storbinary("STOR " + filename, f)
    con.close()
    return 'ftp://kohanova_order:xLBHz_8N@ftp.kohanova.nichost.ru/diploms/' + players + '.docx'

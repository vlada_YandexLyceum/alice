from flask import Flask, request
import logging
import json
import random  # , pymorphy2
from threading import Timer
from function import make_diplom, make_mail, to_make_url

app = Flask(__name__)

# https://tech.yandex.ru/dialogs/alice/doc/sounds/music-docpage/


logging.basicConfig(level=logging.INFO)

quest_text_list = {
    "1": [
        "Убийство, совершенное одной из НИХ, описано Конан Дойлом. Жертвами другой ИХ разновидности стали в 1880 г. 66 австралийцев. А какая характерная особенность внешности была у той, чье имя все ОНИ носят? ",
        "", "зме /волос", "Имелась в виду Медуза Гаргона, а у неё были змеи вместо волос"],
    "2": [
        "Известно, что великий Пеле неоднократно выступал в качестве певца. Интересно, что первая грампластинка с записью песен в его исполнении была выпущена в Советском Союзе. Бразильские газеты сочли это национальным позором, ударом по престижу национальной журналистики. Одна из газет писала: 'Первую пластинку Короля издали не мы, а русские. Да еще и умудрились...' Какое обстоятельство особо возмутило бразильскую прессу?",
        "", "не/плат!не/дал/ден/ьг ег", "Они не заплатили денег"],
    "3": [
        "Эти два слова, арабское и греческое, в переводе означают почти одно и то же, хотя одно из них – географический термин, а другое – воинское звание. Что же это за слова? ",
        "", "адмирал /архипелаг ", "'Адмирал' (ар.) – повелитель морей и 'архипелаг' (гр.) – главный на море"],
    "4": [
        " Что, по мнению В.В.Набокова, делают люди, у которых слишком мало воображения, чтобы писать романы, и слишком короткая память, чтобы писать правду? ",
        "", "пиш пис сочин/мемуар автобиограф", "Пишут мемуары, автобиографию"],
    "5": [
        "Многие не верят в ЕЁ существование. Однако Кант считал, что с НЕЁ начинается любое человеческое знание. А еще говорят, что ОНА подводит только тех, у кого ОНА есть. Назовите ЕЁ. ",
        "", "интуиция", "Это интуиция"],
    "6": [
        "Ни в Стамбуле, ни в Каире ИХ уже нет, зато ОНИ есть в Куала-Лумпуре, Маскате и Бандар-Сери-Бегаване. А какое решение принял в итоге один балбес, поставив себя на ИХ место? ",
        "", "не/женат!холост", "Юрий Никулин был бы холостой"],
    "7": [
        "'В начале восемнадцатого века при дворе султанов дрессировали черепах - их приучали не стоять на месте, а всё время двигаться. Как использовали черепах во время придворных вечеров на открытом воздухе?",
        "", "свет освещ свеч/панцир спин",
        "Им прекрепляли на спины свечи и использовали как движущиеся светильники"],
    "8": ["Какая женщина не спала ночью 2 года, 8 месяцев и 4 недели?", "", "шех/р/зада",
          "Эту известную восточную сказочницу звали Шехерезада"],
    "9": [
        "Чёрный ящик: Она изготовлена мужчинами, но используют ЕЁ женщины. Этому обману уже более 200 лет, но мы до сих пор продолжаем платить за него деньги. Что находится в ящике?",
        "", "бижутерия",
        "Это бижутерия – украшения, изготовленные ювелирами из недрагоценных камней и металлов"],
    "10": [
        "Радикально-авангардное объединение художников начала прошлого века, отколовшееся от «Бубнового валета», носило необычное название из двух слов, обозначающее предмет, который однажды был найден на кусту колючей травы, название которого похоже на чих, и подарен законному владельцу. Как называлось это объединение?",
        "", "осл/хвост", "'ослиный хвост', а владельцем такого предмета был ослик Иа - герой мультфильма 'Винни Пух'"],
    "11": [
        "В каждом из НИХ есть что-то нечеловеческое, механистичное. При этом первый настроен к окружающим дружелюбно, хотя некая хозяйственной женщина от НЕГО немало пострадала. Второй же наоборот весьма недружелюбен, но некой женщине удалось в итоге избежать угрозы и даже получить помощь с ЕГО стороны. Интересно, что оба давали одинаковые обещания, которые стали ИХ 'визитными карточками'. Кто же они?",
        "", "карлсон / терминатор",
        "Имелись в виду Карлсон и Терминатор, которые обещали вернуться. Эти фразы: «Он улетел, но обещал вернуться» и «I’ll be back»"],
    "12": ["Жители одного племени называют это животное «лягушка с крышкой»", "", "черепаха", "Это черепаха"],
    "13": [
        "Почти двадцать лет назад прошел некий футбольный матч. Он был вполне обычным, однако был воспет в некой песне.",
        "", "аргентин/ямайк", "Какая боль, какая боль, Аргентина-Ямайка — 5:0!"]

}

quest_image_list = {
    "1": ["Что изображено на картинке?", "1652229/b8bef13f2e1ae5e0c931", "что/где/когда", "Что? Гдн? Когда?"],
    "2": ["О чём хотел сказать автор этой картины?", "997614/c6cbc0453420fd98e9f0", "самопожертвова",
          "Это весы над океаном. Если они потеряют равновесие, то голубь в клетке умрёт. Поэтому свободный голюбь не улетает, чтобы его собрат остался жить. Картина о самопожертвовании."],
    "3": ["Какое учреждение зашифровано в этой пиктограмме?", "1030494/d2330b22169659d91c97", "наход/бюр",
          "Бюро находок"],
    "4": [
        "Узоры, сделанные по заказу на обручальных кольца, хпохожи, но неидентичны. Что означают узоры на этих кольцах?",
        "213044/05d37cc24fdb34852093", "голос/жених муж/невест жен",
        "Это осциллограммы записи голосов жениха и невесты: «согласен» и «согласна»."],
    "5": ["Как с помощью японской шляпы воины-ниндзя проводили испытания на скорость?", "213044/9761ad174aa5abc53dfc",
          "груд/бе/г ж/шляп/возд",
          "При сдаче зачета бежать нужно так быстро, чтобы встречный поток воздуха удерживал шляпу на груди у воина, у ниндзя (без рук)."]

}

True_messange_for_users_list = [' Да, вы оказались правы!']
False_messange_for_users_list = [' Вы ответили не внрно.']

sessionStorage = {}
Buttons_list = [
    {
        'title': 'Да',
        'hide': True
    },
    {
        'title': 'Нет',
        'hide': True
    }
]


@app.route('/post', methods=['POST'])
def main():
    logging.info('Request: %r', request.json)
    response = {
        'session': request.json['session'],
        'version': request.json['version'],
        'response': {
            'end_session': False
        }
    }
    handle_dialog(response, request.json)
    logging.info('Request: %r', response)
    return json.dumps(response)


def handle_dialog(res, req):
    user_id = req['session']['user_id']
    team_name = ''
    if req['session']['new']:
        res['response'][
            'text'] = 'Здравствуйте! Эта игра создана как командная, но можно играть и одному. Назовите название вашей команды!'
        sessionStorage[user_id] = {
            'score': [0, 0],
            'timer_reaction': True,
            'state_of_dialogue': 1,  # 0 - задан вопрос, 1 - получен ответ
            'team_name': None,  # здесь будет храниться название команды
            'game_started': False,  # здесь информация о том, что пользователь начал игру. По умолчанию False
            'guessed_quests': None,
            'want_diplom': None
        }
        return
    if sessionStorage[user_id]['team_name'] is None:
        # Удостовеяемся, что пользователь ввёл нужное ему назвение команды
        team_name = get_team_name(req)
        sessionStorage[user_id]['team_name'] = team_name
        res['response']['text'] = f'Ваша команда называется "{team_name}"?'
        res['response']['buttons'] = Buttons_list
        return
    if sessionStorage[user_id]['guessed_quests'] is None:
        if 'нет' in req['request']['nlu']['tokens']:
            # Если в первый раз он ввел не верное название, то запрашиваем его повторно
            res['response']['text'] = 'Назовите название вашей команды'
            sessionStorage[user_id]['team_name'] = get_team_name(req)
        else:
            # создаём пустой массив, в который будем номера вопросов, которые пользователь уже отгадал
            sessionStorage[user_id]['guessed_quests'] = []
            # как видно из предыдущего навыка, сюда мы попали, потому что пользователь написал своем имя.
            # Предлагаем ему сыграть и два варианта ответа "Да" и "Нет".
            if sessionStorage[user_id]['team_name'] == 'нет':
                sessionStorage[user_id]['team_name'] = get_team_name(req)
            team_name = sessionStorage[user_id]['team_name']
            res['response'][
                'text'] = f'Добрый день, уважаемые дамы и господа, сегодня против вас будут играть учники ЯндексЛицея. Хочу напомнить правила нашей игры: я задаю вопрос, который выбирается случайным образом на нашей виртуальной рулетке, и у вас есть полторы минуты на прочтение и размышление, после чего вы должны дать ответ. Игра продолжается до шести очков. Если в конце игры счёт 6:0 в пользу знатаков, то команда внаграждается дипломом победителей игры "Что? Где? Когда?" Ну чтож, {team_name},начинаем?'
            res['response']['buttons'] = Buttons_list
    else:
        score = sessionStorage[user_id]['score']
        # У нас уже есть имя, и теперь мы ожидаем ответ на предложение сыграть.
        # В sessionStorage[user_id]['game_started'] хранится True или False в зависимости от того,
        # начал пользователь игру или нет.
        if not sessionStorage[user_id]['game_started']:
            # игра не начата, значит мы ожидаем ответ на предложение сыграть.
            wate_to_start_game(res, req, user_id)
        else:
            play_game(res, req)


def play_game(res, req):
    user_id = req['session']['user_id']
    score = sessionStorage[user_id]['score']

    def timer_reaction():
        sessionStorage[user_id]['timer_reaction'] = False

    if 6 not in score:
        messange = ''
        if sessionStorage[user_id]['state_of_dialogue'] == 0:
            # Попадаем сюда, если задан вопрос и ожидается ответ
            messange, winner, score = asked_question_and_waiting_for_answer(req, user_id, score)

        if sessionStorage[user_id]['state_of_dialogue'] == 1:
            # Сюда, когда генерируем ответ
            generate_a_response(user_id)

        sessionStorage[user_id]['state_of_dialogue'] = 0
        if 6 not in score:
            # Повторная проверка нужна, тк
            # В двух условиях выше изменяется score (счёт),
            # поэтому его надо переправерить
            make_request(user_id, res, messange)

            # Отправляем новый вопрос
            sessionStorage[user_id]['timer'] = Timer(450.0, timer_reaction)
            sessionStorage[user_id]['timer'].start()
        else:
            # Сюда заходим если появляется победитель и отправляем сообщение с итогами
            message_about_end_of_play(res, score, sessionStorage[user_id]['team_name'])

    elif score == [6, 0]:
        if absolutly_winner(res, req) and sessionStorage[user_id]['want_diplom'] is None:
            # Проверка, что пользователь согласился
            # на печать дипломов
            sessionStorage[user_id]['want_diplom'] = True

        elif sessionStorage[user_id]['want_diplom'] == True:
            # Запрос адреса почты и создание диплома
            make_diplom(sessionStorage[user_id]['team_name'], preparing_names_for_printing(req['request']['command']),
                        res['session']['user_id'], req['request']['command'])
            res['response'][
                'text'] = 'А теперь введите адрес электроной почты, либо отправьте сообщение "ссылка", и я отправлю вам ссылку на диплом'
            sessionStorage[user_id]['want_diplom'] = False
        else:
            # Отправка диплома
            if 'ссылка' in req['request']['nlu']['tokens']:
                res['response'][
                    'text'] = 'Перейдите по ссылке и скачайте документ, название у которого - список введённых вами ранее имён \n Ссылка: \n ' + \
                              'я бы её отправила, но PythonAnywhere не даёт, так что, все притензии к нему'
                # to_make_url('я, питон')
            else:
                res['response'][
                    'text'] = 'Грамата отправлена вам на почту, но вы её не найдёте)) \nВсе притензии к PythonAnywhere '
                # make_mail(res['session']['user_id'] + '.docx', ''.join(req['request']['command'].split()))

    sessionStorage[user_id]['score'] = score


def get_first_name(req):
    # перебираем сущности
    for entity in req['request']['nlu']['entities']:
        # находим сущность с типом 'YANDEX.FIO'
        if entity['type'] == 'YANDEX.FIO':
            # Если есть сущность с ключом 'first_name', то возвращаем её значение.
            # Во всех остальных случаях возвращаем None.
            return entity['value'].get('first_name', None)


def get_team_name(req):
    return req['request']['original_utterance']


def check_content(req, answer):
    # Проверка ответа на правильность
    # заключается в том, что у каждого вопроса есть список частей слов,
    # которые должны быть в ответе
    # пример: "не/женат!холост", а ответ "Был бы холостой"
    # знаками '!' в строке разделяются возможные формулировки
    # знаками '/' в каждой формулировке отделены обязательные элементы
    # знаками ' ' отделены синонимы (один из них обязательно должен быть в строке ответа)
    req = ''.join(req)
    answer = answer.split('!')
    boole = True
    G_boole = False
    for rules in answer:
        rules = rules.split('/')
        for rul_elm in rules:
            if any(list(map(lambda x: x.lower() in req, rul_elm.split()))):
                boole = True
            else:
                boole = False
                break
        if G_boole == False:
            G_boole = boole
    return G_boole


def message_about_end_of_play(res, score, team_name):
    # Объявление итогов игры
    if score == [6, 0]:
        res['response'][
            'text'] = f'Наша игра завершена, со счётом {score[0]}/{score[1]} в пользу команды {team_name}. \nИ я поздравляю абсолютных победителей нашей игры! Дамы и господа, не останавливайтесь на достигнутом, может вам стоит поиграть в Элитарном Клубе "Что? Где? Когда?", подумайте над этим, ищите пути развития. И у вас есть возможность получить диплом победителей игры, написав на нём свои имена - вам нужен диплом?'
    elif score[0] == 6:
        res['response'][
            'text'] = f'Наша игра завершена, со счётом {score[0]}/{score[1]} в пользу команды {team_name}. Я поздравляю вас и желаю дальнейших успехов, развития, неприрывного роста. И чтобы всё, что вы знаете, вам когда-нибудь пригодилось в жизни!'
        res['end_session'] = True
    else:
        res['response'][
            'text'] = f'Наша игра завершена, со счётом {score[0]}/{score[1]} в пользу команды ЯндексЛицея. Вопросы были сложные, нужно было рассуждать и нестандартно, и логически. И я желаю вам не расстраиваться, идти дальше. Думаю, если вы наберётесь опыта в подобных играх, то и победать в них будете чаще.'
        res['end_session'] = True


def absolutly_winner(res, req):
    # проверка, что пользователю нужен диплом
    if 'да' in req['request']['nlu']['tokens']:
        res['response'][
            'text'] = 'Отправтье сообщение с перечисленными через запятую именами'
        return True
    else:
        res['response']['text'] = 'Что ж, досвидания!'
        return False


def wate_to_start_game(res, req, user_id):
    if 'да' in req['request']['nlu']['tokens']:
        # если пользователь согласен, то проверяем ни ответил нли он уже на все опросы.
        # По схеме можно увидеть, что здесь окажутся и пользователи, которые уже отгадывали города
        if len(sessionStorage[user_id]['guessed_quests']) == len(quest_text_list.keys()):
            # если все вопросы заданы, то заканчиваем игру
            res['response']['text'] = 'Оно залезло туда, куда залазить не должно'
        else:
            # если есть неотвеченые вопросы, то продолжаем игру
            sessionStorage[user_id]['game_started'] = True
            # функция, которая осуществляет игру
            play_game(res, req)
    elif 'нет' in req['request']['nlu']['tokens']:
        res['response']['text'] = 'Приходите, когда созреете!'
        res['end_session'] = True
    else:
        res['response']['text'] = 'Не поняла ответа! Так да или нет?'


def asked_question_and_waiting_for_answer(req, user_id, score):
    # состояние меняется на 'получен ответ'
    sessionStorage[user_id]['state_of_dialogue'] = 1
    # проверка на правильность ответа и то, что отведённое на ответ время не закончилось
    if check_content(req['request']['nlu']['tokens'], sessionStorage[user_id]['quest_content'][2]) and \
            sessionStorage[user_id]['timer_reaction']:
        messange = random.choice(True_messange_for_users_list)
        score[0] += 1
    else:
        # если время закончилось, то ответ автоматически считается неверным
        if not sessionStorage[user_id]['timer_reaction']:
            sessionStorage[user_id]['timer_reaction'] = True
            messange = 'Время, отведённное на ответ закончилось'
        else:
            messange = random.choice(False_messange_for_users_list)
        score[1] += 1
    # обновление таймера
    sessionStorage[user_id]['timer_reaction'] = True
    # Формирование сообщения о предыдущем вопросе и текст следующего вопроса
    winner = 'команды Янлекс.Лицея' if score[1] > score[0] else 'знатоков' if score[0] > score[1] else ''
    messange = f"Ввнимение, правильный ответ: \n {sessionStorage[user_id]['quest_content'][3]}. \n {messange} \n Счёт {score[0]}/{score[1]} в пользу {winner}.\n\n"
    return messange, winner, score


def generate_a_response(user_id):
    # если это первый вопрос - то он должен быть с картинкой
    if sessionStorage[user_id]['score'] == [0, 0]:
        quest_list = quest_image_list
    # все остальные вопросы текстовые
    else:
        quest_list = quest_text_list
    # Выбор вопроса и проверка, что он ещё не был задан
    quest_num = random.choice(list(quest_list.keys()))
    while quest_num in sessionStorage[user_id]['guessed_quests']:
        quest_num = random.choice(list(quest_list.keys()))
    sessionStorage[user_id]['guessed_quests'].append(quest_num)
    quest_content = quest_list[quest_num]
    sessionStorage[user_id]['quest_content'] = quest_content


def make_request(user_id, res, messange):
    # если в вопросе есть картинка
    if sessionStorage[user_id]['quest_content'][1] != '':
        res['response']['card'] = {}
        res['response']['text'] = '...'
        res['response']['card']['type'] = 'BigImage'
        res['response']['card']['title'] = messange + sessionStorage[user_id]['quest_content'][0]
        res['response']['card']['image_id'] = sessionStorage[user_id]['quest_content'][1]
        res['response']['buttons'] = Buttons_list
    # если в вопросе только текст
    else:
        res['response']['text'] = messange + sessionStorage[user_id]['quest_content'][0]
        res['response']['buttons'] = Buttons_list


def preparing_names_for_printing(name_string):
    # Функция для проверки и, если нужно, преобразования
    # строки с именами (имена в дипломе должны занимать не более чем 5 столбцов)
    # если имён <= 15, то диплом будет корректным (на одной странице)
    # иначе список имён растынется на следующую страницу
    name_list = list(map(lambda x: ' '.join(x.split()), name_string.split(',')))
    shot_name_list = []
    line_of_any_names = ''
    if 5 < len(name_list) <= 10:
        divider = 2
    elif 5 < len(name_list) <= 15:
        divider = 3
    else:
        divider = 1
    for i in range(len(name_list)):
        print(line_of_any_names)
        if i % divider == 0:
            line_of_any_names = name_list[i]

        elif i % divider == divider - 1:
            line_of_any_names += '     -' + name_list[i] if len(line_of_any_names) != 1 else line_of_any_names
            shot_name_list.append(line_of_any_names)

        else:
            line_of_any_names += '     -' + name_list[i]
        if divider == 1 or i == len(name_list) - 1:
            shot_name_list.append(line_of_any_names)

    name_list = shot_name_list
    return name_list
